package com.example.markers.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.markers.App;
import com.example.markers.DatabaseHelper;
import com.example.markers.R;
import com.example.markers.fragment.MapFragment;
import com.example.markers.service.CoordinateLoader;

public class MainActivity extends AppCompatActivity {

    // Будет необходим во фрагментах для создания диалога
    Context activityContext;

    // Объявим здесь на тот случай, когда будет необходимость сообщить фрагменту отрисоваться
    MapFragment mapFragment;

    // Создадим BroadcastReceiver для прослушки сервиса на событие о загрузке данных
    BroadcastReceiver br;
    public final static String PARAM_STATUS = "status";
    public final static String BROADCAST_ACTION = "ru.startandroid.develop.p0961servicebackbroadcast";
    public final static int LOAD_COMPLETE = 100;

    private DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mSqLiteDatabase;
    String dataBaseName = "mydatabase.db";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityContext = this;

        // Как только придет уведомление о загрузке данных, сообщим фрагменту о готовности
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                FragmentStartDraw();
            }
        };
        // создаем фильтр для BroadcastReceiver
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        // регистрируем (включаем) BroadcastReceiver
        registerReceiver(br, intFilt);

        setContentView(R.layout.activity_main);

        mapFragment = new MapFragment();
        mapFragment.setFragmentManager(getFragmentManager());
        mapFragment.setActivityContext(activityContext);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, mapFragment).commit();


        mDatabaseHelper = new DatabaseHelper(App.getAppContext(), dataBaseName, null, 1);
        mSqLiteDatabase = mDatabaseHelper.getReadableDatabase();
        Cursor cursor = mSqLiteDatabase.query("marks", new String[]{DatabaseHelper.LATITUDE_COLUMN,
                        DatabaseHelper.LONGITUDE_COLUMN, DatabaseHelper.DESCRIPTION_COLUMN,
                        DatabaseHelper.IMAGE_COLUMN, DatabaseHelper.LASTVISIT_COLUMNS},
                null, null,
                null, null, null) ;

        // При наличии данных в базе, говорим о готовности фрагменту
        // Иначе запускаем сервис, для загрузки данных впервые
        if(cursor.getCount() >= 1)
        {
            mapFragment.LoadData();
        }
        else
        {
            startService(new Intent(this, CoordinateLoader.class));
        }
    }

    // Метод вызывается при завершении загрузки данных сервисом
    private void FragmentStartDraw()
    {
        mapFragment.LoadData();
        mapFragment.DrawMarks();
    }
}
