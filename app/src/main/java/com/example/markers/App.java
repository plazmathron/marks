package com.example.markers;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static App instance;
    private static Context appContext;

    public App() {
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }
}
