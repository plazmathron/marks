package com.example.markers;

/**
 * Интерфейс для передачи координаты, требующей добавления, или редактирования в базе
 */
public interface RefreshPointListener {
    void refreshPoint(Location location);

}
