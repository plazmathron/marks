package com.example.markers;

/**
 * Добавил класс для хранения информации о метке
 */
public class Location {

    //latitude - широта
    private Double latitude;
    public Double getLatitude(){
        return latitude;
    }
    public void setLatitude(Double latitude){
        this.latitude = latitude;
    }

    //longitude - долгота
    private Double longitude;
    public Double getLongitude(){
        return longitude;
    }
    public void setLongitude(Double longitude){
        this.longitude = longitude;
    }

    //text - описание
    private String text;
    public String getText(){
        return text;
    }
    public void setText(String text){
        this.text = text;
    }

    //image - изображение местности
    private byte[] image;
    public byte[] getImage(){
        return image;
    }
    public void setImage(byte[] image){
        this.image = image;
    }

    //lastVisited - дата посещения.
    private long lastVisited;
    public long getLastVisited(){
        return lastVisited;
    }
    public void setLastVisited(long lastVisited){
        this.lastVisited = lastVisited;
    }

    public Location(Double latitude, Double longitude, String text, byte[] image, long lastVisited) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.text = text;
        this.image = image;
        this.lastVisited = lastVisited;
    }
}
