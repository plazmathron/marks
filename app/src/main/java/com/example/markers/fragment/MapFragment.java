package com.example.markers.fragment;

import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.markers.App;
import com.example.markers.DatabaseHelper;
import com.example.markers.Location;
import com.example.markers.R;
import com.example.markers.RefreshPointListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Фрагмент для работы с картой
 */
public class MapFragment extends Fragment implements RefreshPointListener{

    private GoogleMap googleMap;
    FragmentManager fm;
    Context activityContext;

    // Храним список объектов Location для быстрого доступа к данным
    // Обновляем при загрузке, и при добавлении точки
    HashMap<Integer, Location> marks = new HashMap<>();

    // Создадим BroadcastReceiver чтобы быть проинформированными сервисом о первой загрузке координат

    private DatabaseHelper mDatabaseHelper = new DatabaseHelper(App.getAppContext(), "mydatabase.db", null, 1);
    private SQLiteDatabase mSqLiteDatabase;

    //Осуществляем привязку элементов
    @Bind(R.id.mapFragment)
    MapView mapView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("MyApp", "onCreateView");
        View view = inflater.inflate(R.layout.map_fragment, null);
        ButterKnife.bind(this, view);

        MapsInitializer.initialize(getActivity());

        mapView.onCreate(savedInstanceState);

        // Вызываем окно редактирования при клике по координате
        mapView.getMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // Перебираем точки, если точка имеется в базе, открываем окно редактирования
                for (int i = 0; i < marks.size(); i++) {
                    if (marks.get(i).getLatitude() == marker.getPosition().latitude &&
                            marks.get(i).getLongitude() == marker.getPosition().longitude) {
                        DescriptionFragment descriptionFragment = new DescriptionFragment();
                        descriptionFragment.setLocation(marks.get(i));
                        descriptionFragment.setFragmentManager(fm);
                        descriptionFragment.setActivityContext(activityContext);
                        descriptionFragment.setContext(MapFragment.this);
                        //открываем фрагмент с редактированем параметров метки
                        fm.beginTransaction().replace(R.id.fragment_container, descriptionFragment).addToBackStack("descriptionFragment").commit();
                        return true;
                    }
                }
                return false;
            }
        });

        // Добавляем новую точку псоредством долгого нажатия на карту
        mapView.getMap().setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                DescriptionFragment descriptionFragment = new DescriptionFragment();
                // Фото, Описание оставляем пустыми, время установим текущее
                Location location = new Location(latLng.latitude, latLng.longitude, "", new byte[0], System.currentTimeMillis());
                descriptionFragment.setLocation(location);
                descriptionFragment.setFragmentManager(fm);
                descriptionFragment.setActivityContext(activityContext);
                descriptionFragment.setContext(MapFragment.this);
                //открываем фрагмент с редактированем параметров метки
                fm.beginTransaction().replace(R.id.fragment_container, descriptionFragment).addToBackStack("descriptionFragment").commit();
            }
        });
        initializeMap();
        return view;
    }

    // Реализуем интерфейс RefreshPointListener
    public void refreshPoint(Location location)
    {
        // true если точка новая
        Boolean newObject = true;
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();

        // Вначале проверим коодинату на наличие в базе
        for (int i=0; i<marks.size(); i++) {
            if(location.getLatitude() == marks.get(i).getLatitude() &&
                    location.getLongitude() == marks.get(i).getLongitude())
            {
                //Если мы найдем такую координату, нам ее необходимо обновить
                marks.get(i).setText(location.getText());
                marks.get(i).setImage(location.getImage());
                marks.get(i).setLastVisited(location.getLastVisited());

                //не забываем обновить значения в базу
                ContentValues values = new ContentValues();
                values.put(DatabaseHelper.DESCRIPTION_COLUMN, location.getText());
                values.put(DatabaseHelper.IMAGE_COLUMN, location.getImage());
                values.put(DatabaseHelper.LASTVISIT_COLUMNS, location.getLastVisited());

                mSqLiteDatabase.update("marks", values, "_id=" + i, null);
                newObject = false;
            }
        }
        // В случае если точка новая, делаем запись в базу
        if (newObject)
        {
            marks.put(marks.size(), location);
            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.LATITUDE_COLUMN, location.getLatitude());
            values.put(DatabaseHelper.LONGITUDE_COLUMN, location.getLongitude());
            values.put(DatabaseHelper.DESCRIPTION_COLUMN, location.getText());
            values.put(DatabaseHelper.IMAGE_COLUMN, location.getImage());
            values.put(DatabaseHelper.LASTVISIT_COLUMNS, location.getLastVisited());/*dateFormat.format(lastVisited)*/
            // Вставляем данные в таблицу
            mSqLiteDatabase.insert("marks", null, values);
            LatLng myLaLn = new LatLng(location.getLatitude(), location.getLongitude());
            mapView.getMap().addMarker(new MarkerOptions().position(myLaLn));
        }
    }

    @Override
    public void onDestroyView() {
        Log.d("MyApp", "map fragment onDestroy");
        super.onDestroyView();
        mapView.onDestroy();
        googleMap = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        initializeMap();
    }

    public boolean initializeMap() {
        if (googleMap == null) {
            Log.d("MyApp", "map is null");
            googleMap = mapView.getMap();
            if (googleMap == null) {
                Toast.makeText(getActivity(), getString(R.string.initialize_map_error), Toast.LENGTH_SHORT).show();
                return false;
            }
            // Отрисовываем метки на карте
            DrawMarks();
        }
        return true;
    }

    // Заполним таблицу с точками значениями из базы
    private void LoadDataFromDB() {
        try {
            mSqLiteDatabase = mDatabaseHelper.getReadableDatabase();
            Cursor cursor = mSqLiteDatabase.query("marks", new String[]{DatabaseHelper.LATITUDE_COLUMN,
                            DatabaseHelper.LONGITUDE_COLUMN, DatabaseHelper.DESCRIPTION_COLUMN,
                            DatabaseHelper.IMAGE_COLUMN, DatabaseHelper.LASTVISIT_COLUMNS},
                    null, null,
                null, null, null) ;

            int i=0;
            if(cursor.getCount() >= 1) {
                while (cursor.moveToNext()) {
                    Double latitude = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.LATITUDE_COLUMN));
                    Double longitude = cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.LONGITUDE_COLUMN));
                    String description = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DESCRIPTION_COLUMN));
                    byte[] image = cursor.getBlob(cursor.getColumnIndex(DatabaseHelper.IMAGE_COLUMN));
                    long lastVisit = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.LASTVISIT_COLUMNS));
                    Location location = new Location(latitude, longitude, description, image, lastVisit);
                    marks.put(i, location);
                    i++;
                }
            }
            cursor.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // Отрисовываем метки на карте, воспользуемся данными из таблицы
    public void DrawMarks()
    {
        for (int i=0; i<marks.size(); i++) {
            LatLng myLaLn = new LatLng(marks.get(i).getLatitude(), marks.get(i).getLongitude());
            mapView.getMap().addMarker(new MarkerOptions().position(myLaLn));
        }
    }

    // Вызываем метод из главного акитивити.
    // Сразу же, если имеются записи в базе
    // или после того как пришло уведомление от сервиса о загрузке точек
    public void LoadData()
    {
        LoadDataFromDB();
    }

    public void setFragmentManager(FragmentManager fm) { this.fm = fm; }

    // activityContext будет необходим для отображения диалога в фрагменте с настройками
    public void setActivityContext(Context context)
    {
        this.activityContext = context;
    }
}
