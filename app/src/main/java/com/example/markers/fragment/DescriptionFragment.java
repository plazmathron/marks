package com.example.markers.fragment;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.markers.App;
import com.example.markers.DatabaseHelper;
import com.example.markers.Location;
import com.example.markers.R;
import com.example.markers.RefreshPointListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Фрагмент для работы с параметрами отметки
 */
public class DescriptionFragment extends Fragment {

    // Текущая координата
    Location location;

    private DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mSqLiteDatabase;

    // Необходим для создания диалога
    Context activityContext;
    // Необходим для возврата к преыдущему фрагменту в стеке
    FragmentManager fm;

    private RefreshPointListener refreshPointListener;

    // На экран будем выводить координаты с 2 знаками после запятой
    DecimalFormat decimalFormat = new DecimalFormat("#0.00");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    // Будем выводить пользователю дату в текущем формате
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    String dataBaseName = "mydatabase.db";

    Menu optionsMenu;

    // Ограничим число воодимых символов в описание к координате
    InputFilter descriptionFilter = new InputFilter.LengthFilter(25);

    // Данные вводимые пользователем контролируем с помощью маски, представленной ниже
    InputFilter coordinateFilter = new InputFilter() {

        private Pattern pattern =  Pattern.compile("-?[0-9]{1,3}([.])([0-9]{1,2})?");

        @Override
        public CharSequence filter(CharSequence source,
                                   int sourceStart, int sourceEnd,
                                   Spanned destination, int destinationStart,
                                   int destinationEnd)
        {
            String textToCheck = destination.subSequence(0, destinationStart).
                    toString() + source.subSequence(sourceStart, sourceEnd) +
                    destination.subSequence(
                            destinationEnd, destination.length()).toString();

            Matcher matcher = pattern.matcher(textToCheck);
            if(!matcher.matches()){
                if(!matcher.hitEnd()){
                    return "";
                }
            }
            return null;
        }
    };

    //Осуществляем привязку элементов
    @Bind(R.id.add_photo_button)
    ImageView map_picture;

    @Bind(R.id.delete_photo_button)
    ImageView delete_photo;

    @Bind(R.id.descriptionEditText)
    AppCompatEditText adress;

    @Bind(R.id.longetude)
    AppCompatEditText longetude;

    @Bind(R.id.latitude)
    AppCompatEditText latitude;

    @Bind(R.id.date)
    AppCompatEditText lastVisited;

    @Bind(R.id.date_button)
    ImageView date_button;

    @Bind(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;

    @OnClick(R.id.date_button) void onEditDateLastVisited() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, myCallBack,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @OnClick(R.id.add_photo_button) void onEditMapPhotoButtonClick() {
        openGallery();
    }

    @OnClick(R.id.delete_photo_button) void onDeleteMapPhotoButtonClick() {
        location.setImage(new byte[0]);
        map_picture.setImageDrawable(ContextCompat.getDrawable(App.getAppContext(), R.drawable.add_photo_icon));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("MyApp", "onCreateView");

        View view = inflater.inflate(R.layout.description_layout, null);
        ButterKnife.bind(this, view);

        mDatabaseHelper = new DatabaseHelper(App.getAppContext(), dataBaseName, null, 1);
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();

        // На случай если точка только создана, или изображение не задано, ничего не выводим
        if (location.getImage().length > 0) {
            Bitmap bmp = BitmapFactory.decodeByteArray(location.getImage(), 0, location.getImage().length);
            map_picture.setImageBitmap(bmp);
        }

        if (toolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Зададим разделитель для десятичных дробей
        custom.setDecimalSeparator('.');
        decimalFormat.setDecimalFormatSymbols(custom);

        // Назначаем фильтры для вводимых полей
        adress.setFilters(new InputFilter[]{descriptionFilter});
        adress.setText(location.getText());

        longetude.setFilters(new InputFilter[]{coordinateFilter});
        // Будем контролировать чтобы число лежало в допустимом диапазоне, в случае чего уведомлять пользователя
        longetude.addTextChangedListener(new TextWatcher() {
            // Будем хранить исходную строку, на тот случай если нас что-то не устроит
            String sourceString;

            private Boolean allowableRange(String s) {
                // на случай если пользователю вздумается оставить строку пустой, или же оставить "-"
                // присекаем это сразу
                return !(s.matches("[-]") || s.matches("")) && ((Double.parseDouble(s) > -180 && Double.parseDouble(s) < 180));
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sourceString = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!allowableRange(editable.toString())) {
                    Toast toast = Toast.makeText(App.getAppContext(), getString(R.string.longetude_allowableRange), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    longetude.setText(sourceString);
                }
            }
        });
        longetude.setText(decimalFormat.format(location.getLongitude()));

        latitude.setFilters(new InputFilter[]{coordinateFilter});
        latitude.addTextChangedListener(new TextWatcher() {
            String sourceString;

            private Boolean allowableRange(String s) {
                // на случай если пользователю вздумается оставить строку пустой, или же оставить "-"
                // присекаем это сразу
                return !(s.matches("[-]") || s.matches("")) && ((Double.parseDouble(s) > -180 && Double.parseDouble(s) < 180));
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sourceString = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!allowableRange(charSequence.toString())) {
                    Toast toast = Toast.makeText(App.getAppContext(), getString(R.string.latitude_allowableRange), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!allowableRange(editable.toString())) {
                    latitude.setText(sourceString);
                }
            }
        });
        latitude.setText(decimalFormat.format(location.getLatitude()));

        String date = dateFormat.format(new Date(location.getLastVisited()));
        lastVisited.setText(date);

        return view;
    }

    // По установке даты для последнего посещения, формируем новую, и обновляем значение в поле
    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                String dateString = String.valueOf(dayOfMonth) + "." + String.valueOf(monthOfYear+1) + "." + String.valueOf(year);
                java.util.Date date = dateFormat.parse(dateString);
                lastVisited.setText(" " + dateFormat.format(date));
                // Обновим значение в потенциальной новой/обнволенной координате
                location.setLastVisited(date.getTime());
            }
            catch (Exception ex)
            { ex.printStackTrace();}
        }
    };

    // Откроем галерею
    public void openGallery() {
        Intent photoGrabIntent = new Intent(Intent.ACTION_PICK);
        photoGrabIntent.setType("image/*");
        photoGrabIntent.setAction(Intent.ACTION_GET_CONTENT);
        try {
            startActivityForResult(Intent.createChooser(photoGrabIntent, getString(R.string.photo_grab_intent)), getTargetRequestCode());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // Обновим фото в объекте location и на фрагменте
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bit;
        try{
            if (data==null) return;
            Uri imageUri = data.getData();
            bit = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(imageUri));
            map_picture.setImageBitmap(bit);
            // Обновим фото для потенциально новой/обнволенной координаты
            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bit.compress(Bitmap.CompressFormat.PNG,100, baos);
            byte [] b=baos.toByteArray();
            location.setImage(b);
        } catch(IOException ioEx) {
            ioEx.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu, menu);
        optionsMenu = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home)
        {
            fm.popBackStack();
        }
        if (id == R.id.homeAsUp)
        {
            fm.popBackStack();
        }
        if (id == android.R.id.home)
        {
            fm.popBackStack();
        }
        if (id == R.id.done_button) {

            location.setLatitude(Double.parseDouble(latitude.getText().toString()));
            location.setLongitude(Double.parseDouble(longetude.getText().toString()));
            location.setText(adress.getText().toString());
            refreshPointListener.refreshPoint(location);
            fm.popBackStack();
        }

        else if (id == R.id.map_fragment_button) {
            fm.popBackStack();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setContext(RefreshPointListener refreshPointListener) { this.refreshPointListener = refreshPointListener; }

    public void setFragmentManager(FragmentManager fm) { this.fm = fm; }

    public void setLocation(Location location)
    {
        this.location = location;
    }

    public void setActivityContext(Context context) { this.activityContext = context; }
}


