package com.example.markers.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;

import com.example.markers.DatabaseHelper;
import com.example.markers.activity.MainActivity;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

/**
 * Сервис для загрузки точек
 */
public class CoordinateLoader extends Service {

    final String LOG_TAG = "myLogs";

    // Имена полей будем хранить в переменных, на тот случай если имена поменяются
    String plaseColumn = "places";
    String latitudeColumn = "latitude";
    String longtitudeColumn = "longtitude";
    String textColumn = "text";
    String imageColumn = "image";
    String lastVisitedColumn = "lastVisited";

    HttpURLConnection urlConnection = null;
    BufferedReader reader = null;

    String JSONUrl = "http://interesnee.ru/files/android-middle-level-data.json";

    // Таблица для хранения изображений, перед передачей в базу
    HashMap<Integer, byte[]> markImages = new HashMap<>();


    private DatabaseHelper mDatabaseHelper;
    private SQLiteDatabase mSqLiteDatabase;
    String dataBaseName = "mydatabase.db";
    String tableName = "marks";

    // Храним маску, которую будем использовать в дальнейшем для приведения даты к необходимому виду
    String dateMask = "EEE MMM dd HH:mm:ss Z yyyy";

    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");

        mDatabaseHelper = new DatabaseHelper(this, dataBaseName, null, 1);
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();

        // Запускаем метод по разбору jSon в отдельном потоке
        new Thread(new Runnable() {
            public void run() {
                ParsJSON();
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return null;
    }

    protected void ParsJSON() {
        String resultJson = "";
        JSONObject dataJsonObj = null;

        try {
            URL url = new URL(JSONUrl);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            resultJson = buffer.toString();
            dataJsonObj = new JSONObject(resultJson);

            JSONArray places = dataJsonObj.getJSONArray(plaseColumn);
            for (int i = 0; i < places.length(); i++) {
                JSONObject place = places.getJSONObject(i);

                // Начинаем формировать данные для записи в базу
                Double latitude = place.getDouble(latitudeColumn);
                Double longtitude = place.getDouble(longtitudeColumn);
                String text = place.getString(textColumn);

                // Переменная необходимая для работы метода по загрузке фото
                final Integer finalElement = places.length();
                // Фото к отметке нам так же предоставляется в виде URL
                final String imageAdress = place.getString(imageColumn);
                final Integer id = i;
                // Т.к. загрузка фото может затянуться, запускаем ее в отдельном потоке.
                // Снимки в базу поместим позже
                Thread myThread = new Thread(
                        new Runnable() { // описываем объект Runnable в конструкторе
                            public void run() {
                                GetBitmapFromURL(id, imageAdress, finalElement);
                            }
                        }
                );
                myThread.start();


                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateMask, Locale.US);
                java.util.Date date = simpleDateFormat.parse(place.getString(lastVisitedColumn));
                long lastVisited = date.getTime();

                // Формируем данные для записи в базу
                ContentValues values = new ContentValues();
                values.put(DatabaseHelper._ID, i);
                values.put(DatabaseHelper.LATITUDE_COLUMN, latitude);
                values.put(DatabaseHelper.LONGITUDE_COLUMN, longtitude);
                values.put(DatabaseHelper.DESCRIPTION_COLUMN, text);
                values.put(DatabaseHelper.LASTVISIT_COLUMNS, lastVisited);
                // Вставляем данные в таблицу
                mSqLiteDatabase.insert(tableName, null, values);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Метод для получение изображения из URL
    // id - неоьходим для записи в базу
    // finalElement - необходим для сообещения об окончании загрузки всех снимков
    public void GetBitmapFromURL(Integer id, String urlString, Integer finalElement) {
        InputStream is = null;
        URL url = null;
        try {
            url = new URL(urlString);
            is = url.openStream ();
            byte[] imageBytes = IOUtils.toByteArray(is);
            // Заполняем таблицу с изображениями
            markImages.put(id, imageBytes);
            // по готовности вызываем метод для запизи в базу
            if (markImages.size() == finalElement) LoadBitmapToDB();
        }
        catch (IOException e) {
            e.printStackTrace ();
        }
        finally {
            if (is != null)
            try { is.close(); }
            catch (IOException e) {e.printStackTrace();}
        }
    }

    // Метод, вызываемый при окончании загрузки изображений
    private void LoadBitmapToDB()
    {
        Intent intent = new Intent(MainActivity.BROADCAST_ACTION);
        try {
            for (int i = 0; i < markImages.size(); i++) {
                // Помещаем загруженные снимки в базу
                ContentValues values = new ContentValues();
                values.put(DatabaseHelper.IMAGE_COLUMN, markImages.get(i));
                mSqLiteDatabase.update(tableName, values, "_id="+i, null);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        // Даем основному потоку знать о завершении загрузки
        intent.putExtra(MainActivity.PARAM_STATUS, MainActivity.LOAD_COMPLETE);
        sendBroadcast(intent);
    }
}
